import express from 'express';
import http from 'http';
import bodyParser from 'body-parser';
import cors from 'cors';
import Routes from './routes/route';


const app = express();

app.use(cors());
app.use(bodyParser({limit:'50mb'}));
app.use('/gamasome',Routes)


const httpServer = http.createServer(app);
httpServer.listen(8000,err=>{
    return err 
        ?console.error('Failed to start http server')
        :console.error('Server is running in 8000 port')
})