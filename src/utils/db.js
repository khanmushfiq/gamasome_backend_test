import {MongoClient} from 'mongodb';
import MongoUriBuilder from 'mongo-uri-builder';

//Mongo connection variables
const mongoConnectionSettings = {
    host: 'localhost',
    port: '27017',
    username: '',
    password: '',
    database: 'gamasomeDB'
};
let mongoCon={}

//Mongo db connection.
const mongoConnect = ()=>{
    return new Promise((resolve,reject)=>{
        MongoClient.connect(...mongoConnectionSettings,{
            useNewUrlParser:true
        },(err,dbConnection)=>{
                if(err){
                    console.log("Error in mongodb database connection",err);
                    reject(err);
                    return;
                }
                mongoCon[mongoConnectionSettings.database] = dbConnection.db(mongoConnectionSettings.database);
                resolve(mongoCon[mongoConnectionSettings.database]);
        });
    });
}

export default mongoConnect;
