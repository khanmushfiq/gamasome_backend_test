import fetch from 'node-fetch';
import mongoConnect from './db';


const GITHUB_API = 'https://api.github.com';

export default class GamasomBase{

    OnFetchUsers = async ()=>{
        try{
            let fetchUsers = await fetch(`${GITHUB_API}/users`,{
                method: 'GET',
                headers: {'Content-Type': 'application/json'}
            });
            let users = await fetchUsers.json();
            return users;
        }catch(ex){
            console.error('Error in getting github users.');
            throw ex;
        }
    }

    OnFetchSingleUser = async(username)=>{
        try{
            let fetchSingleUser = await fetch(`${GITHUB_API}/users/${username}/repos`,{
                method: 'GET',
                headers:{'Content-Type': 'application/json'}
            });
            let user = await fetchSingleUser.json();
            return user;
        }catch(ex){
            console.error('Error in getting user details.');
            throw ex;
        }
    }
}