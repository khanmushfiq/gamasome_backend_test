import express from 'express';
import GamasomBase from '../utils/gamasome-base';

const router = express.Router();
const gamasomeBase = new GamasomBase();

router.get("/",(req,res)=>{
    res.send({
        success: true,
        message: 'Welcome to gamasome!'
    });
});

router.get('/get-users',async (req,res)=>{
    try{
        let users = await gamasomeBase.OnFetchUsers();
        res.status(200).json({
            success: true,
            message: 'User has been fetched successfully',
            data: users
        })
    }catch(ex){
        console.error('Error in fetching users');
        res.status(400).json({
            success:false,
            message: 'Error in fetching users',
            error: ex
        });
    }
});

router.post('/user-details',async(req,res)=>{
    try{
        let {username} = req.body;
        let userDetails = await gamasomeBase.OnFetchSingleUser(username);
        res.status(200).json({
            success: true,
            message: 'User details has been fetched successfully',
            data: userDetails
        });
    }catch(ex){
        console.error('Error in fetching user details');
        res.status(400).json({
            success: false,
            message: 'Error in getting user details',
            error: ex
        })
    }
})

export default router;